/**
 * @file DeckObject.h
 *
 * @brief Wrapper for Phase 10 deck to use signals
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <QObject>

#include <Phase10/CommonTypes.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       DeckObject
  // Description: Wrapper for Phase 10 deck to use signals
  //-------------------------------------------------------------------------//
  class DeckObject : public QObject
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pDeck - deck to listen to
    /// @param parent - parent object
    ///////////////////////////////////////////////////////////////////////////
    DeckObject(sDeck pDeck, QObject* parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~DeckObject() = default;

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when deck is updated
    /// @param pDeck - deck updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void deckUpdate(sDeck pDeck);

  private:
    // Private class variables
    const sDeck mDeck; ///< deck
  };

} // namespace Phase10
