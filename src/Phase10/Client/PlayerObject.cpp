/**
 * @file PlayerObject.cpp
 *
 * @brief Wrapper for Phase 10 player to use signals
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Client/PlayerObject.h>
#include <Phase10/Player.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  PlayerObject::PlayerObject(sPlayer pPlayer, QObject* parent) : 
    QObject(parent), mPlayer(pPlayer)
  {
    mPlayer->addListener(std::bind(&PlayerObject::playerUpdate, this, mPlayer));
  }

} // namespace Phase10
