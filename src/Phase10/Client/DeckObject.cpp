/**
 * @file DeckObject.cpp
 *
 * @brief Wrapper for Phase 10 deck to use signals
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Client/DeckObject.h>
#include <Phase10/Deck.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  DeckObject::DeckObject(sDeck pDeck, QObject* parent) : 
    QObject(parent), mDeck(pDeck)
  {
    qRegisterMetaType<sDeck>("sDeck");
    mDeck->addListener(std::bind(&DeckObject::deckUpdate, this, mDeck));
  }

} // namespace Phase10
