/**
 * @file Manager.cpp
 *
 * @brief Manager class for Phase 10 client
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Network/Socket.h>
#include <Phase10/Client/Manager.h>
#include <Phase10/Game.h>
#include <Phase10/Messages.h>
#include <Phase10/Player.h>
#include <Phase10/User.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ClientManager::ClientManager(System::sLogger pLogger, sGameFactory pGameFactory, sUserFactory pUserFactory) :
    mLogger(pLogger), mGameFactory(pGameFactory), mUserFactory(pUserFactory)
  {
    mRunning = false;
    mServerUpdate = false;
    mOpenGames = std::make_shared<OpenGamesList>();

    qRegisterMetaType<sGame>("sGame");
    qRegisterMetaType<scOpenGamesList>("scOpenGamesList");
    qRegisterMetaType<sUser>("sUser");
    qRegisterMetaType<sPlayer>("sPlayer");
  }

  /////////////////////////////////////////////////////////////////////////////
  // Destructor
  /////////////////////////////////////////////////////////////////////////////
  ClientManager::~ClientManager()
  {
    stop();
  }

  /////////////////////////////////////////////////////////////////////////////
  // isActive
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::isActive()
  {
    return(mRunning.load() && mSocket && mSocket->valid());
  }

  /////////////////////////////////////////////////////////////////////////////
  // registerUser
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::registerUser(const std::string& name)
  {
    bool success = false;
    if(!isActive())
    {
      mLogger->error("ClientManager::%s - Manager has not been started", __func__);
    }
    else if(mUser)
    {
      mLogger->warn("ClientManager::%s - User has already been registered", __func__);
    }
    else
    {
      auto pMsg = std::make_shared<Messages::RequestUserMsg>();
      pMsg->name = name;
      success = true;

      ServerMsg msg(MSG_REQUEST_USER, std::move(pMsg));
      mSendQueue.add(std::move(msg));

      mLogger->debug("ClientManager::%s - Requested user ID for %s from the server",
                      __func__, name.c_str());
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // requestDeal
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::requestDeal()
  {
    bool success = false;
    if(!isActive())
    {
      mLogger->error("ClientManager::%s - Manager has not been started", __func__);
    }
    else if(mUser == nullptr)
    {
      mLogger->warn("ClientManager::%s - User has not registered yet", __func__);
    }
    else if(mGame == nullptr)
    {
      mLogger->warn("ClientManager::%s - User is not a member of a game", __func__);
    }
    else
    {
      ServerMsg msg(MSG_REQUEST_DEAL);
      mSendQueue.add(std::move(msg));
      success = true;

      mLogger->debug("ClientManager::%s - Requested next round deal", __func__);
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // requestJoin
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::requestJoin(uint32_t gameID)
  {
    bool success = false;
    if(!isActive())
    {
      mLogger->error("ClientManager::%s - Manager has not been started", __func__);
    }
    else if(mUser == nullptr)
    {
      mLogger->warn("ClientManager::%s - User has not registered yet", __func__);
    }
    else if(mGame)
    {
      mLogger->warn("ClientManager::%s - User is already a member of %s [%u]",
                    __func__, mGame->name().c_str(), mGame->id());
    }
    else
    {
      auto pMsg = std::make_shared<Messages::RequestJoinGameMsg>();
      pMsg->gameID = gameID;
      success = true;

      ServerMsg msg(MSG_REQUEST_GAME_JOIN, std::move(pMsg));
      mSendQueue.add(std::move(msg));

      mLogger->debug("ClientManager::%s - Requested %s [%u] to join game %u",
                      __func__, mUser->name().c_str(), mUser->id(), gameID);
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // requestNewGame
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::requestNewGame(const std::string& name)
  {
    bool success = false;
    if(!isActive())
    {
      mLogger->error("ClientManager::%s - Manager has not been started", __func__);
    }
    else if(mUser == nullptr)
    {
      mLogger->warn("ClientManager::%s - User has not registered yet", __func__);
    }
    else if(mGame)
    {
      mLogger->warn("ClientManager::%s - User is already a member of %s [%u]",
                    __func__, mGame->name().c_str(), mGame->id());
    }
    else
    {
      auto pMsg = std::make_shared<Messages::RequestGameMsg>();
      pMsg->name = name;
      success = true;

      ServerMsg msg(MSG_REQUEST_GAME_CREATE, std::move(pMsg));
      mSendQueue.add(std::move(msg));

      mLogger->debug("ClientManager::%s - Requested new game %s from the server",
                      __func__, name.c_str());
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // start
  /////////////////////////////////////////////////////////////////////////////
  bool ClientManager::start(uint32_t port, const std::string& hostname)
  {
    // Connect to server
    bool success = false;
    if(mSocket)
    {
      mLogger->debug("Client already started");
    }
    else
    {
      using namespace std::placeholders;
      Network::Socket::DataCallback dataCallback;
      dataCallback = std::bind(&ClientManager::msgReceived, this, _1, _2);
      mSocket = std::make_shared<Network::Socket>(mLogger, dataCallback, port, hostname);
    }

    // Start threads
    success = mSocket->valid();
    if(success && !mRunning.exchange(true))
    {
      mControlThread = std::thread(&ClientManager::controlThread, this);
      mSendThread = std::thread(&ClientManager::sendThread, this);
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // stop
  /////////////////////////////////////////////////////////////////////////////
  void ClientManager::stop()
  {
    if(mRunning.exchange(false))
    {
      // Stop control thread
      mLogger->debug("ClientManager::%s - Stopping control thread", __func__);
      mMsgQueue.notify();
      if(mControlThread.joinable())
      {
        mControlThread.join();
      }
    
      // Stop sending thread
      mLogger->debug("ClientManager::%s - Stopping sending thread", __func__);
      mSendQueue.notify();
      if(mSendThread.joinable())
      {
        mSendThread.join();
      }

      // Close server socket so we stop accepting connections
      mLogger->debug("ClientManager::%s - Closing socket", __func__);
      mSocket.reset();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // controlThread
  /////////////////////////////////////////////////////////////////////////////
  void ClientManager::controlThread()
  {
    mLogger->debug("ClientManager::%s - Entering", __func__);

    // Local variables
    bool hasGameStarted = false;

    // Function to check for end of server
    auto exitCriteria = [this]()
    {
      return !mRunning.load();
    };

    while(mRunning.load())
    {
      // Wait for next message
      System::sSerialBuffer pMsg;
      if(!mMsgQueue.remove(pMsg, true, exitCriteria) || !mRunning.load())
      {
        mLogger->debug("ClientManager::%s - No message received", __func__);
        continue;
      }

      // Make sure connection is open
      if(pMsg == nullptr || !isActive())
      {
        mLogger->warn("ClientManager::%s - Lost connection with server");
        connectionLost();
        break;
      }

      // Process message
      uint32_t msgID = pMsg->msgID();
      switch(msgID)
      {
        // User registered
        case MSG_NEW_USER:
        {
          Messages::UserMsg msg;
          if(mUser)
          {
            mLogger->warn("ClientManager::%s - Got duplicate user registration", 
                          __func__);
          }
          else if(!msg.decode(*pMsg))
          {
            mLogger->error("ClientManager::%s - Failed to decode new user message",
                            __func__);
          }
          else
          {
            mUser = mUserFactory->create(msg.id, msg.name);
            if(mUser == nullptr)
            {
              mLogger->error("ClientManager::%s - Failed to create new user %s [%u]",
                              __func__, msg.name.c_str(), msg.id);
            }
            else
            {
              mLogger->debug("ClientManager::%s - Created new user %s [%u]",
                              __func__, mUser->name().c_str(), mUser->id());

              userRegistered(mUser);
            }
          }
          break;
        }

        // Open games updated
        case MSG_OPEN_GAMES:
        {
          Messages::OpenGamesMsg msg;
          if(!msg.decode(*pMsg))
          {
            mLogger->error("ClientManager::%s - Failed to decode open games message",
                            __func__);
          }
          else
          {
            auto pOpenGames = std::make_shared<OpenGamesList>();
            for(auto& game : msg.games)
            {
              pOpenGames->emplace_back(game.id, game.name, game.numPlayers, game.state);
            }

            mLogger->debug("ClientManager::%s - Updated open games (%lu)",
                            __func__, pOpenGames->size());

            mOpenGames = pOpenGames;
            openGamesUpdated(mOpenGames);
          }
          break;
        }

        // Received game members
        case MSG_GAME_MEMBERS:
        {
          Messages::GameMembersMsg msg;
          if(mUser == nullptr)
          {
            mLogger->error("ClientManager::%s - Received game members before user registered", __func__);
          }
          else if(!msg.decode(*pMsg))
          {
            mLogger->error("ClientManager::%s - Failed to decode game members message",
                            __func__);
          }
          else
          {
            // Find open game
            auto it = std::find_if(mOpenGames->begin(), mOpenGames->end(),
              [&msg](const GameInfo& info)
            {
              return(info.id() == msg.gameID);
            });

            if(it == mOpenGames->end())
            {
              mLogger->error("ClientManager::%s - Received game members for non-existent game %u",
                              __func__, msg.gameID);
            }
            else
            {
              // Update game members
              auto pGame = mGameFactory->findOrCreate(it->id(), it->name());
              if(pGame)
              {
                //TODO: Signal GUI of error if we get members after game started
                mServerUpdate = true;
                pGame->setMembers(msg, *mUserFactory);
                mServerUpdate = false;

                if(mUser->game() == mGame.get())
                {
                  mLogger->debug("ClientManager::%s - Updated members for %s [%u]",
                                  __func__, mGame->name().c_str(), mGame->id());
                }
                else if(mUser->game() == pGame.get())
                {
                  hasGameStarted = false;
                  mGame = pGame;
                  gameJoined(mGame);

                  mLogger->info("ClientManager::%s - Successfully added %s [%u] to %s [%u]",
                                  __func__, mUser->name().c_str(), mUser->id(),
                                  mGame->name().c_str(), mGame->id());
                }
                else
                {
                  mLogger->warn("ClientManager::%s - %s [%u] is not a member of %s [%u]",
                                 __func__, mUser->name().c_str(), mUser->id(),
                                 pGame->name().c_str(), pGame->id());
                }
              }
              else
              {
                mLogger->error("ClientManager::%s - Failed to create game %s [%u]",
                                __func__, it->name().c_str(), it->id());
              }
            }
          }
          break;
        }

        // Game state update
        case MSG_GAME_UPDATE:
        {
          Messages::GameStateMsg msg;
          if(mUser == nullptr)
          {
            mLogger->warn("ClientManager::%s - Got game state update before registering user", __func__);
          }
          else if(mGame == nullptr)
          {
            mLogger->warn("ClientManager::%s - Got game state update before joining game", __func__);
          }
          else if(!msg.decode(*pMsg))
          {
            mLogger->warn("ClientManager::%s - Failed to decode game state update", __func__);
          }
          else
          {
            // Suppress listener so the client doesn't send the updated state back
            mLogger->debug("ClientManager::%s - Updating state for %s [%u]",
              __func__, mGame->name().c_str(), mGame->id());

            mServerUpdate = true;
            mGame->setState(msg);
            mServerUpdate = false;

            // Add player listeners
            if(!hasGameStarted)
            {
              hasGameStarted = true;
              mGame->addPlayerListener(std::bind(&ClientManager::gameUpdate, this, mGame));

              auto myPlayer = mGame->getPlayer(mUser->id());
              if(myPlayer)
              {
                gameStarted(mGame, myPlayer);
              }
              else
              {
                mLogger->error("ClientManager::%s - Failed to find player for %s [%u]",
                                __func__, mUser->name().c_str(), mUser->id());
              }
            }

            mLogger->debug("ClientManager::%s - Updated %s [%u] state from server",
                            __func__, mGame->name().c_str(), mGame->id());
          }
          break;
        }

        // Unhandled message ID
        default:
        {
          mLogger->error("ClientManager::%s - Received message with unrecognized ID %u",
                          __func__, msgID);
          break;
        }
      }
    }

    mLogger->debug("ClientManager::%s - Exiting", __func__);
  }

  /////////////////////////////////////////////////////////////////////////////
  // gameUpdate
  /////////////////////////////////////////////////////////////////////////////
  void ClientManager::gameUpdate(scGame pGame)
  {
    if(pGame == mGame && !mServerUpdate.load())
    {
      mLogger->debug("ClientManager::%s - %s [%u]", __func__,
        pGame->name().c_str(), pGame->id());

      auto pMsg = std::make_shared<Messages::GameStateMsg>();
      pGame->getState(*pMsg);

      ServerMsg msg(MSG_GAME_UPDATE, std::move(pMsg));
      mSendQueue.add(std::move(msg));
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // msgReceived
  /////////////////////////////////////////////////////////////////////////////
  void ClientManager::msgReceived(Network::Socket* pSocket, System::sSerialBuffer pMsg)
  {
    // Add to message queue
    if(pSocket == mSocket.get())
    {
      mMsgQueue.add(std::move(pMsg));
    }
    else
    {
      mLogger->error("ClientManager::%s - Got message from another socket", __func__);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // sendThread
  /////////////////////////////////////////////////////////////////////////////
  void ClientManager::sendThread()
  {
    mLogger->debug("ClientManager::%s - Entering", __func__);

    // Local variables
    ServerMsg            sendMsg;
    System::SerialBuffer msgBuffer;

    // Function to check for end of server
    auto exitCriteria = [this]()
    {
      return !mRunning.load();
    };

    while(mRunning.load())
    {
      // Get next message
      if(!mSendQueue.remove(sendMsg, true, exitCriteria) || !mRunning.load())
      {
        continue;
      }

      // Check for socket open
      if(!isActive())
      {
        mLogger->warn("ClientManager::%s - Socket is closed", __func__);
        continue;
      }

      // Encode message
      msgBuffer.clear();
      msgBuffer.setMsgID(sendMsg.id);

      if(sendMsg.pMsg)
      {
        sendMsg.pMsg->encode(msgBuffer);
      }

      // Send message
      if(!mSocket->sendMsg(msgBuffer))
      {
        mLogger->error("ClientManager::%s - Failed to send message with ID %u", __func__, sendMsg.id);
      }
    }

    mLogger->debug("ClientManager::%s - Exiting", __func__);
  }

} // namespace Phase10
