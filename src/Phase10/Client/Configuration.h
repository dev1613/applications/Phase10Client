/**
 * @file Configuration.h
 *
 * @brief Class to hold Phase 10 client configuration
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <System/Logger.h>

namespace rapidxml
{
  template <class Ch = char>
  class xml_node;
}

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       ClientConfiguration
  // Description: Class to hold Phase 10 client configuration
  //-------------------------------------------------------------------------//
  struct ClientConfiguration
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    ClientConfiguration();

    // Struct variables
    std::string      hostname; ///< server host
    uint32_t         port;     ///< server port
    System::LogLevel logLevel; ///< logging level
  };

  //-------------------------------------------------------------------------//
  // Class:       ClientConfigurationParser
  // Description: Class to parse Phase 10 client configuration
  //-------------------------------------------------------------------------//
  class ClientConfigurationParser
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    ///////////////////////////////////////////////////////////////////////////
    ClientConfigurationParser(System::sLogger pLogger);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ClientConfigurationParser() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the configuration
    /// @return - configuration data
    ///////////////////////////////////////////////////////////////////////////
    const ClientConfiguration& config() const { return mConfig; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Parses the configuration
    /// @param configFile - XML config file
    /// @return - true if parsed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool parse(const std::string& configFile);

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    ClientConfiguration mConfig; ///< config data

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Parses the configuration tree
    /// @param root - root node
    /// @return - true if parsed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool parseTree(const rapidxml::xml_node<>& root);
  };

} // namespace Phase10
