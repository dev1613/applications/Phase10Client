/**
 * @file Configuration.h
 *
 * @brief Class to hold Phase 10 client configuration
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <rapidxml_utils.hpp>

#include <Phase10/Client/Configuration.h>

// Constants
static const std::string KEY_HOST("Host");
static const std::string KEY_LOG_LEVEL("LogLevel");
static const std::string KEY_PORT("Port");

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ClientConfiguration::ClientConfiguration() :
    hostname("localhost"), port(0), logLevel(System::LOG_INFO)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ClientConfigurationParser::ClientConfigurationParser(System::sLogger pLogger) :
    mLogger(pLogger)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // parse
  /////////////////////////////////////////////////////////////////////////////
  bool ClientConfigurationParser::parse(const std::string& configFile)
  {
    bool success = false;

    mLogger->info("ClientConfigurationParser::%s - Parsing configuration at %s",
                  __func__, configFile.c_str());

    try
    {
      // Parse file
      rapidxml::file<> xmlFile(configFile.c_str());
      auto pDoc = std::make_shared<rapidxml::xml_document<>>();
      pDoc->parse<0>(xmlFile.data());

      // Parse tree
      auto pRootNode = pDoc->first_node();
      if(pRootNode)
      {
        success = parseTree(*pRootNode);
      }
    }
    catch(std::exception& ex)
    {
      mLogger->error("Error while parsing %s: %s", configFile.c_str(), ex.what());
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // parseTree
  /////////////////////////////////////////////////////////////////////////////
  bool ClientConfigurationParser::parseTree(const rapidxml::xml_node<>& root)
  {
    // Parse tree
    bool foundPort = false;
    bool foundHost = false;

    for(auto* pChild = root.first_node(); pChild; pChild = pChild->next_sibling())
    {
      if(pChild->type() == rapidxml::node_element)
      {
        if(pChild->name() == KEY_LOG_LEVEL)
        {
          System::GetLogLevel(mConfig.logLevel, pChild->value());
        }
        else if(pChild->name() == KEY_PORT)
        {
          mConfig.port = std::stoul(pChild->value());
          foundPort = true;
        }
        else if(pChild->name() == KEY_HOST)
        {
          mConfig.hostname = pChild->value();
          foundHost = true;
        }
        else
        {
          mLogger->warn("ClientConfigurationParser::%s - Unrecognized tag %s in tree",
                        __func__, pChild->name());
        }
      }
    }

    // Must have port and host
    if(!foundPort)
    {
      mLogger->error("ClientConfigurationParser::%s - Missing %s in config file",
                    __func__, KEY_PORT.c_str());
    }

    if(!foundHost)
    {
      mLogger->error("ClientConfigurationParser::%s - Missing %s in config file",
                      __func__, KEY_HOST.c_str());
    }

    return(foundPort && foundHost);
  }

} // namespace Phase10
