/**
 * @file PlayerObject.h
 *
 * @brief Wrapper for Phase 10 player to use signals
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <QObject>

#include <Phase10/CommonTypes.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       PlayerObject
  // Description: Wrapper for Phase 10 player to use signals
  //-------------------------------------------------------------------------//
  class PlayerObject : public QObject
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pPlayer - player to listen to
    /// @param parent - parent object
    ///////////////////////////////////////////////////////////////////////////
    PlayerObject(sPlayer pPlayer, QObject* parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~PlayerObject() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the player
    /// @return - player
    ///////////////////////////////////////////////////////////////////////////
    sPlayer player() const { return mPlayer; }

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when player is updated
    /// @param pPlayer - player updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void playerUpdate(sPlayer pPlayer);

  private:
    // Private class variables
    const sPlayer mPlayer; ///< player
  };

} // namespace Phase10
