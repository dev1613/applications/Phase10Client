/**
 * @file Manager.h
 *
 * @brief Manager class for Phase 10 client
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <atomic>
#include <functional>
#include <list>

#include <QObject>

#include <Common/ThreadSafeQueue.h>
#include <Network/Common.h>
#include <Phase10/CommonTypes.h>
#include <System/Logger.h>
#include <System/SerialBuffer.h>
#include <System/Serializable.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       ServerMsg
  // Description: Struct for message to send to server
  //-------------------------------------------------------------------------//
  struct ServerMsg
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param _id - message ID
    /// @param _pMsg - message to send
    ///////////////////////////////////////////////////////////////////////////
    ServerMsg(uint32_t _id = 0, System::sSerializable _pMsg = nullptr) :
      id(_id), pMsg(_pMsg) {}

    // Struct variables
    uint32_t              id;   ///< message ID
    System::sSerializable pMsg; ///< message data
  };

  //-------------------------------------------------------------------------//
  // Class:       ClientManager
  // Description: Manager class for Phase 10 client
  //-------------------------------------------------------------------------//
  class ClientManager : public QObject
  {
    Q_OBJECT

  public:
    // Public types
    using OpenGamesList   = std::list<GameInfo>;
    using sOpenGamesList  = std::shared_ptr<OpenGamesList>;
    using scOpenGamesList = std::shared_ptr<const OpenGamesList>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pGameFactory - game factory
    /// @param pUserFactory - user factory
    ///////////////////////////////////////////////////////////////////////////
    ClientManager(System::sLogger pLogger, sGameFactory pGameFactory, sUserFactory pUserFactory);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ClientManager();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Checks if the manager is active
    /// @return - true if active
    ///////////////////////////////////////////////////////////////////////////
    bool isActive();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game the user is a member of
    /// @return - user's game
    ///////////////////////////////////////////////////////////////////////////
    sGame getGame() const { return mGame; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the currently open games
    /// @return - open games
    ///////////////////////////////////////////////////////////////////////////
    scOpenGamesList getOpenGames() const { return mOpenGames; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the user
    /// @return - user
    ///////////////////////////////////////////////////////////////////////////
    sUser getUser() const { return mUser; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Requests a user ID from the server
    /// @param name - new user name
    /// @return - true if successfully requested, else false
    ///////////////////////////////////////////////////////////////////////////
    bool registerUser(const std::string& name);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Requests that the next round of the active game be dealt
    /// @return - true if successfully requested, else false
    ///////////////////////////////////////////////////////////////////////////
    bool requestDeal();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Requests that the user join an existing game
    /// @param gameID - game ID
    /// @return - true if successfully requested, else false
    ///////////////////////////////////////////////////////////////////////////
    bool requestJoin(uint32_t gameID);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Requests that the server create a new game
    /// @param name - new game name
    /// @return - true if successfully requested, else false
    ///////////////////////////////////////////////////////////////////////////
    bool requestNewGame(const std::string& name);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Starts the client
    /// @param port - port number to connect to server
    /// @param hostname - server hostname
    /// @return - true if started
    ///////////////////////////////////////////////////////////////////////////
    bool start(uint32_t port, const std::string& hostname);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Stops the client
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void stop();

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the server connection is lost
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void connectionLost();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a game is joined
    /// @param pGame - game joined
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameJoined(sGame pGame);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a game is started
    /// @param pGame - game joined
    /// @param pPlayer - player associated with this user
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameStarted(sGame pGame, sPlayer pPlayer);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the open games are updated
    /// @param pOpenGames - open games
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void openGamesUpdated(scOpenGamesList pOpenGames);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the user is registered
    /// @param pUser - new user
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void userRegistered(sUser pUser);

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    const sGameFactory mGameFactory; ///< game factory
    const sUserFactory mUserFactory; ///< user factory

    Network::sSocket mSocket;    ///< socket connected to server
    sUser            mUser;      ///< user for this client
    sGame            mGame;      ///< game the user is a member of
    sOpenGamesList   mOpenGames; ///< open games

    Common::ThreadSafeQueue<System::sSerialBuffer> mMsgQueue;  ///< input message queue
    Common::ThreadSafeQueue<ServerMsg>             mSendQueue; ///< send message queue

    std::atomic<bool> mServerUpdate;  ///< server state update in progress
    std::atomic<bool> mRunning;       ///< server running?
    std::thread       mControlThread; ///< control thread
    std::thread       mSendThread;    ///< sending thread

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread to handle control processing
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void controlThread();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the draw pile or discard pile is updated
    /// @param pGame - game updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameUpdate(scGame pGame);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a message is received from a socket
    /// @param pSocket - socket message was received from
    /// @param pMsg - message data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void msgReceived(Network::Socket* pSocket, System::sSerialBuffer pMsg);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Thread to handle sending out messages
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void sendThread();
  };

} // namespace Phase10
