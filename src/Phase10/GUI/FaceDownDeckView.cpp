/**
 * @file Phase10FaceDownDeckView.cpp
 *
 * @brief Classes to represent graphics view that displays a face down deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Deck.h>
#include <Phase10/GUI/CardImage.h>
#include <Phase10/GUI/FaceDownDeckView.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  FaceDownDeckView::FaceDownDeckView(System::sLogger pLogger, sDeck pHand, size_t fitCards, QWidget *parent) :
    DeckView(pLogger, pHand, fitCards, parent)
  {
    refreshScene(pHand);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  FaceDownDeckView::FaceDownDeckView(System::sLogger pLogger, sDeck pHand, QWidget *parent) :
    FaceDownDeckView(pLogger, pHand, 10, parent)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // getCardImage
  /////////////////////////////////////////////////////////////////////////////
  scCardImage FaceDownDeckView::getCardImage(const Card& card) const
  {
    return CardImageFactory::getInstance().getFaceDown();
  }

} // namespace Phase10
