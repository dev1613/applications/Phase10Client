/**
 * @file Widget.cpp
 *
 * @brief Main widget class for Phase 10 client GUI
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <QBoxLayout>
#include <QDialog>
#include <QExposeEvent>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#include <Common/EntryBox.h>
#include <Phase10/Client/Configuration.h>
#include <Phase10/Client/Manager.h>
#include <Phase10/GUI/OpenGamesTableView.h>
#include <Phase10/GUI/GameWidget.h>
#include <Phase10/GUI/Widget.h>
#include <Phase10/Game.h>
#include <Phase10/Player.h>
#include <Phase10/User.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Widget::Widget(System::sLogger pLogger, const std::string& configFile,
                 sGameFactory pGameFactory, sUserFactory pUserFactory) :
    mLogger(pLogger)
  {
    // Start manager
    mManager = std::make_shared<Phase10::ClientManager>(mLogger, pGameFactory, pUserFactory);
    startManager(configFile);

    // Create layout items
    initializeLayout();
  }

  /////////////////////////////////////////////////////////////////////////////
  // connectionLost
  /////////////////////////////////////////////////////////////////////////////
  void Widget::connectionLost()
  {
    mOpenGamesDialog->hide();
    mRegisterDialog->hide();
    mErrorDialog->show();
  }

  /////////////////////////////////////////////////////////////////////////////
  // createErrorDialog
  /////////////////////////////////////////////////////////////////////////////
  void Widget::createErrorDialog()
  {
    // Initialize dialog
    mErrorDialog = new QDialog(this);
    mErrorDialog->setWindowTitle("Error");

    auto* layout = new QVBoxLayout(this);
    mErrorDialog->setLayout(layout);

    // Error label
    auto* errorLabel = new QLabel("Lost connection with the server!", mErrorDialog);
    layout->addWidget(errorLabel);

    // OK button
    auto* okButton = new QPushButton("OK", mErrorDialog);
    layout->addWidget(okButton, 0, Qt::AlignCenter);

    // Set connections
    connect(okButton, &QPushButton::clicked, mErrorDialog, &QDialog::accept);
    connect(mErrorDialog, &QDialog::accepted, this, &QWidget::close);
    connect(mErrorDialog, &QDialog::rejected, this, &QWidget::close);
  }

  /////////////////////////////////////////////////////////////////////////////
  // createGameWidget
  /////////////////////////////////////////////////////////////////////////////
  void Widget::createGameWidget()
  {
    mGameWidget = new GameWidget(mLogger, this);
    mGameWidget->hide();
    connect(mGameWidget, &GameWidget::deal, mManager.get(), &ClientManager::requestDeal);
    connect(mGameWidget, &GameWidget::quit, this, &Widget::gameQuit);
  }

  /////////////////////////////////////////////////////////////////////////////
  // createOpenGamesDialog
  /////////////////////////////////////////////////////////////////////////////
  void Widget::createOpenGamesDialog()
  {
    // Initialize dialog
    mOpenGamesDialog = new QDialog(this);
    mOpenGamesDialog->setWindowTitle("Open Games");

    auto* layout = new QVBoxLayout(this);
    mOpenGamesDialog->setLayout(layout);

    // Open games table
    auto* table = new OpenGamesTableView(mLogger, mManager, mOpenGamesDialog);
    layout->addWidget(table, 0, Qt::AlignCenter);

    // Cancel button
    auto* cancelButton = new QPushButton("Cancel", mOpenGamesDialog);
    layout->addWidget(cancelButton, 0, Qt::AlignCenter);

    // Set connections
    connect(cancelButton, &QPushButton::clicked, mOpenGamesDialog, &QDialog::reject);
    connect(mOpenGamesDialog, &QDialog::rejected, this, &QWidget::close);
  }

  /////////////////////////////////////////////////////////////////////////////
  // createQuitDialog
  /////////////////////////////////////////////////////////////////////////////
  void Widget::createQuitDialog()
  {
    // Initialize dialog
    mQuitDialog = new QDialog(this);
    mQuitDialog->setWindowTitle("Quit");

    auto* layout = new QVBoxLayout(this);
    mQuitDialog->setLayout(layout);

    // Error label
    auto* quitLabel = new QLabel("Too many players have left the game!", mQuitDialog);
    layout->addWidget(quitLabel);

    // OK button
    auto* okButton = new QPushButton("OK", mQuitDialog);
    layout->addWidget(okButton, 0, Qt::AlignCenter);

    // Set connections
    connect(okButton, &QPushButton::clicked, mQuitDialog, &QDialog::accept);
    connect(mQuitDialog, &QDialog::accepted, this, &QWidget::close);
    connect(mQuitDialog, &QDialog::rejected, this, &QWidget::close);
  }

  /////////////////////////////////////////////////////////////////////////////
  // createRegisterDialog
  /////////////////////////////////////////////////////////////////////////////
  void Widget::createRegisterDialog()
  {
    // Constants
    static const char * const STATUS_ERROR_TEXT   = "Name is invalid";
    static const char * const STATUS_FAIL_TEXT    = "Failed to register user";
    static const char * const STATUS_SUCCESS_TEXT = "Waiting on server...";

    // Initialize dialog
    mRegisterDialog = new QDialog(this);
    mRegisterDialog->setWindowTitle("Register");

    auto* layout = new QVBoxLayout(this);
    mRegisterDialog->setLayout(layout);

    // Welcome label
    auto* welcomeLabel = new QLabel("Welcome to Phase 10! Enter your name below", this);
    layout->addWidget(welcomeLabel);

    // Status label
    auto* statusLabel = new QLabel(this);
    layout->addWidget(statusLabel);

    // Name entry box
    auto* nameWidget = new QWidget(this);
    auto* nameLayout = new QHBoxLayout(nameWidget);
    nameWidget->setLayout(nameLayout);
    layout->addWidget(nameWidget, 0, Qt::AlignCenter);

    auto* nameLabel = new QLabel("Name: ", nameWidget);
    nameLayout->addWidget(nameLabel);

    auto* nameEntry = new Common::EntryBox(nameWidget);
    nameEntry->setMaximumHeight(25);
    nameLayout->addWidget(nameEntry);

    // Buttons
    auto* buttonWidget = new QWidget(this);
    auto* buttonLayout = new QHBoxLayout(buttonWidget);
    buttonWidget->setLayout(buttonLayout);
    layout->addWidget(buttonWidget, 0, Qt::AlignCenter);

    auto* okButton = new QPushButton("OK", buttonWidget);
    buttonLayout->addWidget(okButton);

    auto* cancelButton = new QPushButton("Cancel", buttonWidget);
    buttonLayout->addWidget(cancelButton);

    // Register function
    auto registerUser = [this, statusLabel, nameEntry, okButton]()
    {
      std::string name = nameEntry->toPlainText().toStdString();
      if(name.empty())
      {
        statusLabel->setText(STATUS_ERROR_TEXT);
      }
      else if(!mManager->registerUser(name))
      {
        statusLabel->setText(STATUS_FAIL_TEXT);
      }
      else
      {
        statusLabel->setText(STATUS_SUCCESS_TEXT);
        okButton->setEnabled(false);
        nameEntry->setEnabled(false);
      }
    };

    // Set connections
    connect(nameEntry, &Common::EntryBox::editingFinished, registerUser);
    connect(okButton, &QPushButton::clicked, registerUser);
    connect(cancelButton, &QPushButton::clicked, mRegisterDialog, &QDialog::reject);
    connect(mRegisterDialog, &QDialog::rejected, this, &QWidget::close);
  }

  /////////////////////////////////////////////////////////////////////////////
  // gameQuit
  /////////////////////////////////////////////////////////////////////////////
  void Widget::gameQuit()
  {
    mOpenGamesDialog->hide();
    mRegisterDialog->hide();
    mQuitDialog->show();
  }

  /////////////////////////////////////////////////////////////////////////////
  // gameStarted
  /////////////////////////////////////////////////////////////////////////////
  void Widget::gameStarted(sGame pGame, sPlayer pPlayer)
  {
    mLogger->debug("Widget::%s - Started %s [%u] as player %s [%u]",
                    __func__, pGame->name().c_str(), pGame->id(),
                    pPlayer->name().c_str(), pPlayer->id());

    mOpenGamesDialog->accept();
    mGameWidget->initializeGame(pGame, pPlayer);
    mGameWidget->setFixedSize(size());
    mGameWidget->show();
  }

  /////////////////////////////////////////////////////////////////////////////
  // initializeLayout
  /////////////////////////////////////////////////////////////////////////////
  void Widget::initializeLayout()
  {
    // Create dialog boxes
    createErrorDialog();
    createQuitDialog();
    createRegisterDialog();
    createOpenGamesDialog();
    createGameWidget();

    // Start manager
    if(mManager->isActive())
    {
      mLogger->debug("Widget::%s - Manager is active", __func__);
      mRegisterDialog->open();
    }
    else
    {
      mLogger->error("Widget::%s - Failed to start manager", __func__);
      mErrorDialog->open();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // resizeEvent
  /////////////////////////////////////////////////////////////////////////////
  void Widget::resizeEvent(QResizeEvent* pEvent)
  {
    mLogger->debug("Widget::%s - %dx%d", __func__,
      pEvent->size().width(), pEvent->size().height());

    if(mGameWidget)
    {
      mGameWidget->setFixedSize(pEvent->size());
    }

    QWidget::resizeEvent(pEvent);
  }

  /////////////////////////////////////////////////////////////////////////////
  // startManager
  /////////////////////////////////////////////////////////////////////////////
  void Widget::startManager(const std::string& configFile)
  {
    // Set callbacks
    using namespace std::placeholders;
    connect(mManager.get(), &ClientManager::userRegistered, this, &Widget::userRegistered);
    connect(mManager.get(), &ClientManager::gameStarted, this, &Widget::gameStarted);
    connect(mManager.get(), &ClientManager::connectionLost, this, &Widget::connectionLost);

    // Parse configuration
    ClientConfigurationParser parser(mLogger);
    mLogger->info("Widget::%s - Parsing config from %s", __func__, configFile.c_str());
    if(!parser.parse(configFile))
    {
      mLogger->error("Widget::%s - Failed to parse %s", __func__, configFile.c_str());
      return;
    }

    // Set logger level
    auto& config = parser.config();
    mLogger->setLevel(config.logLevel);

    // Start manager
    mLogger->info("Widget::%s - Attempting to connect to server at %s port %u",
                  __func__, config.hostname.c_str(), config.port);

    if(!mManager->start(config.port, config.hostname))
    {
      mLogger->error("Widget::%s - Failed to launch client manager", __func__);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // userRegistered
  /////////////////////////////////////////////////////////////////////////////
  void Widget::userRegistered(sUser pUser)
  {
    mLogger->debug("Widget::%s - %s [%u]",
      __func__, pUser->name().c_str(), pUser->id());
    mRegisterDialog->accept();
    mOpenGamesDialog->setWindowTitle(pUser->name().c_str());
    mOpenGamesDialog->open();
  }

} // namespace Phase10
