/**
 * @file GameWidget.cpp
 *
 * @brief Classes to represent main Phase 10 game GUI
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <QBoxLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QMouseEvent>
#include <QPushButton>
#include <QRadioButton>

#include <Common/GUI_Utilities.h>
#include <Phase10/Card.h>
#include <Phase10/Client/PlayerObject.h>
#include <Phase10/Deck.h>
#include <Phase10/Game.h>
#include <Phase10/GUI/CardImage.h>
#include <Phase10/GUI/FaceDownDeckView.h>
#include <Phase10/GUI/FaceUpDeckView.h>
#include <Phase10/GUI/GameWidget.h>
#include <Phase10/GUI/PhaseView.h>
#include <Phase10/Match.h>
#include <Phase10/Player.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  GameWidget::GameWidget(System::sLogger pLogger, QWidget* parent) :
    QWidget(parent), mLogger(pLogger)
  {
    initializeLayout();
  }

  /////////////////////////////////////////////////////////////////////////////
  // cardDoubleClicked
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::cardDoubleClicked(const Card& card)
  {
    // Note: This will do nothing if not my turn
    mPlayer->discard(card);
  }

  /////////////////////////////////////////////////////////////////////////////
  // cardPressed
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::cardPressed(const Card& card)
  {
    if(mPlayer->getState() == PLAYER_ACTIVE)
    {
      mDragDeck->add(card);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // draw
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::draw(bool fromDiscard)
  {
    // Draw card
    if(fromDiscard)
    {
      mPlayer->drawFromDiscard();
    }
    else
    {
      mPlayer->draw();
    }

    // Disable buttons
    mDrawButton->setEnabled(false);
    mDiscardButton->setEnabled(false);
  }

  /////////////////////////////////////////////////////////////////////////////
  // createPlayer
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::createPlayer(sPlayer pPlayer, int row, int col)
  {
    auto* gridLayout = dynamic_cast<QGridLayout*>(layout());
    auto& controls = mPlayerControls[pPlayer];

    // Create player views
    auto* mainBox = new QWidget(this);
    auto* mainLayout = new QVBoxLayout(mainBox);
    mainBox->setLayout(mainLayout);
    gridLayout->addWidget(mainBox, row, col, Qt::AlignCenter);

    // Reverse layout order for top row
    Qt::Alignment infoBoxAlign;
    if(row == 0)
    {
      mainLayout->setDirection(QBoxLayout::Up);
      infoBoxAlign = Qt::AlignLeft | Qt::AlignTop;
    }
    else
    {
      infoBoxAlign = Qt::AlignLeft | Qt::AlignBottom;
    }

    // Create phase view
    auto* playerObj = new PlayerObject(pPlayer, this);
    auto* phaseView = new PhaseView(mLogger, playerObj, mainBox);
    mainLayout->addWidget(phaseView, 0, Qt::AlignHCenter);

    // Create hand view
    DeckView* handView = nullptr;
    if(pPlayer == mPlayer)
    {
      mPlayerHand = std::make_shared<Deck>();
      pPlayer->getHand()->addListener(std::bind(&GameWidget::updatePlayerHand, this));
      updatePlayerHand();

      handView = new FaceUpDeckView(mLogger, mPlayerHand, mainBox);
      connect(handView, &DeckView::cardDoubleClicked, this, &GameWidget::cardDoubleClicked);
      connect(handView, &DeckView::cardPressed, this, &GameWidget::cardPressed);
    }
    else
    {
      handView = new FaceDownDeckView(mLogger, pPlayer->getHand(), mainBox);
    }

    mainLayout->addWidget(handView, 0, Qt::AlignHCenter);

    // Create info view
    auto* infoBox = new QGroupBox(pPlayer->name().c_str(), this);
    auto* infoLayout = new QVBoxLayout(infoBox);
    infoBox->setLayout(infoLayout);
    gridLayout->addWidget(infoBox, row, col, infoBoxAlign);

    // Create phase number label
    controls.phaseLabel = new QLabel(pPlayer->getPhaseString().c_str(), infoBox);
    infoLayout->addWidget(controls.phaseLabel);

    // Create state label
    controls.stateLabel = new QLabel("IDLE", infoBox);
    infoLayout->addWidget(controls.stateLabel);

    // Create score label
    controls.scoreLabel = new QLabel("Score:", infoBox);
    infoLayout->addWidget(controls.scoreLabel);

    // Create sort mode radio buttons
    if(pPlayer == mPlayer)
    {
      auto* sortByValueButton = new QRadioButton("Sort Value", infoBox);
      sortByValueButton->setChecked(true);
      infoLayout->addWidget(sortByValueButton);
      connect(sortByValueButton, &QRadioButton::toggled, [this](bool checked)
      {
        if(checked)
        {
          mPlayer->setMode(MODE_SORT_VALUE);
        }
      });

      auto* sortByColorButton = new QRadioButton("Sort Color", infoBox);
      infoLayout->addWidget(sortByColorButton);
      connect(sortByColorButton, &QRadioButton::toggled, [this](bool checked)
      {
        if(checked)
        {
          mPlayer->setMode(MODE_SORT_COLOR);
        }
      });
    }
    else if(mGame->numPlayers() > 2)
    {
      // Create skip button
      // Note: For 2 player game, game will internally skip other player
      controls.skipButton = new QPushButton("Skip", infoBox);
      controls.skipButton->setEnabled(false);
      infoLayout->addWidget(controls.skipButton);
      connect(controls.skipButton, &QPushButton::clicked, [this, pPlayer]()
      {
        mPlayer->skipPlayer(*pPlayer);
      });
    }

    // Update fields when player state updates
    connect(playerObj, &PlayerObject::playerUpdate, this, &GameWidget::playerUpdate);
    playerUpdate(pPlayer);
  }

  /////////////////////////////////////////////////////////////////////////////
  // initializeGame
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::initializeGame(sGame pGame, sPlayer pPlayer)
  {
    // TODO: Allow changing game
    if(mGame || mPlayer)
    {
      mLogger->warn("GameWidget::%s - Already in a game", __func__);
      return;
    }

    // Check players
    auto players = pGame->getPlayers();
    if(std::find(players.begin(), players.end(), pPlayer) == players.end())
    {
      mLogger->error("GameWidget::%s - Player %s [%u] is not in game state",
        __func__, pPlayer->name().c_str(), pPlayer->id());
      return;
    }

    // Update tracking
    mGame = pGame;
    mPlayer = pPlayer;

    // Initialize discard pile
    mGame->addDiscardPileListener(std::bind(&GameWidget::updateDiscard, this));
    updateDiscard();

    // Initialize draw pile
    mGame->addDrawPileListener(std::bind(&GameWidget::updateDraw, this));
    updateDraw();

    // Reorder players so current player is first
    while(players.front() != mPlayer)
    {
      players.push_back(players.front());
      players.pop_front();
    }

    // Row/column for various numbers of players
    static const std::map<size_t, std::list<std::pair<int, int>>> PLAYER_LOCATION
    {
      // 2 players
      {
        2,
        {
          std::make_pair(2, 1), // Bottom-center
          std::make_pair(0, 1)  // Top-center
        }
      },

      // 3 players
      {
        3,
        {
          std::make_pair(2, 1), // Bottom-center
          std::make_pair(0, 0), // Top-left
          std::make_pair(0, 2)  // Top-right
        }
      },

      // 4 players
      {
        4,
        {
          std::make_pair(2, 1), // Bottom-center
          std::make_pair(0, 0), // Top-left
          std::make_pair(0, 1), // Top-center
          std::make_pair(0, 2)  // Top-right
        }
      },

      // 5 players
      {
        5,
        {
          std::make_pair(2, 1), // Bottom-center
          std::make_pair(2, 0), // Bottom-left
          std::make_pair(0, 0), // Top-left
          std::make_pair(0, 2), // Top-right
          std::make_pair(2, 2)  // Bottom-right
        }
      },

      // 6 players
      {
        6,
        {
          std::make_pair(2, 1), // Bottom-center
          std::make_pair(2, 0), // Bottom-left
          std::make_pair(0, 0), // Top-left
          std::make_pair(0, 1), // Top-center
          std::make_pair(0, 2), // Top-right
          std::make_pair(2, 2)  // Bottom-right
        }
      }
    };

    // Add players
    auto& playerLoc = PLAYER_LOCATION.at(players.size());
    auto playerIT = players.begin();
    auto locIT = playerLoc.begin();
    for( ; playerIT != players.end() && locIT != playerLoc.end(); ++playerIT, ++locIT)
    {
      auto& row     = locIT->first;
      auto& col     = locIT->second;
      auto& pPlayer = *playerIT;
      createPlayer(pPlayer, row, col);
    }

    // Raise drag card to the top
    mDragView->raise();
  }

  /////////////////////////////////////////////////////////////////////////////
  // initializeLayout
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::initializeLayout()
  {
    // Initialize layout
    auto* layout = new QGridLayout(this);
    setLayout(layout);

    for(int c = 0; c < 3; ++c)
    {
      layout->setColumnStretch(c, 1);
    }

    // Draw pile and discard pile
    auto* centerBox = new QWidget(this);
    auto* centerLayout = new QGridLayout(centerBox);
    centerBox->setLayout(centerLayout);
    centerLayout->setHorizontalSpacing(CardImage::SCALE_WIDTH / 3);
    centerLayout->setVerticalSpacing(10);

    // Discard pile and button
    mDiscardDeck = std::make_shared<Deck>();
    mDiscardView = new FaceUpDeckView(mLogger, mDiscardDeck, 1, centerBox);
    centerLayout->addWidget(mDiscardView, 0, 0, Qt::AlignCenter);

    mDiscardButton = new QPushButton("Draw", centerBox);
    mDiscardButton->setEnabled(false);
    centerLayout->addWidget(mDiscardButton, 1, 0, Qt::AlignCenter);
    connect(mDiscardButton, &QPushButton::clicked, std::bind(&GameWidget::draw, this, true));

    // Draw pile and button
    mDrawDeck = std::make_shared<Deck>();
    auto* drawPileView = new FaceDownDeckView(mLogger, mDrawDeck, 1, centerBox);
    centerLayout->addWidget(drawPileView, 0, 1, Qt::AlignCenter);

    mDrawButton = new QPushButton("Draw", centerBox);
    mDrawButton->setEnabled(false);
    centerLayout->addWidget(mDrawButton, 1, 1, Qt::AlignCenter);
    connect(mDrawButton, &QPushButton::clicked, std::bind(&GameWidget::draw, this, false));

    // Deal button
    mDealButton = new QPushButton("Deal", centerBox);
    mDrawButton->setVisible(false);
    centerLayout->addWidget(mDealButton, 1, 1, Qt::AlignCenter);
    connect(mDealButton, &QPushButton::clicked, this, &GameWidget::deal);

    layout->addWidget(centerBox, 1, 1, Qt::AlignCenter);

    // Drag deck
    mDragDeck = std::make_shared<Deck>();
    mDragView = new FaceUpDeckView(mLogger, mDragDeck, 1, this);
    mDragView->setVisible(false);
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseMoveEvent
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::mouseMoveEvent(QMouseEvent *pEvent)
  {
    QWidget::mouseMoveEvent(pEvent);
    if(!mDragDeck->empty())
    {
      setDragLoc(pEvent->pos());
      mPlayerHand->remove(mDragDeck->front());
      mDragView->setVisible(true);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseReleaseEvent
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::mouseReleaseEvent(QMouseEvent *pEvent)
  {
    // Must hide drag view before calling childAt
    QWidget::mouseReleaseEvent(pEvent);
    bool dragging = mDragView->isVisible();
    mDragView->setVisible(false);

    // Ignore if no card was being dragged
    if(mDragDeck->empty() || !dragging)
    {
      mDragDeck->clear();
      return;
    }

    // Return drag card to the hand
    auto dragCard = mDragDeck->front();
    mDragDeck->passAll(*mPlayerHand);

    // Check if the card was dropped on a phase
    auto* pChild = childAt(pEvent->pos());
    auto* pPhase = Common::findParent<PhaseView>(pChild);

    // If dropped on phase, try to add to the match
    if(pPhase)
    {
      bool front    = false;
      auto childPos = pPhase->mapFromGlobal(pEvent->globalPos());
      auto pMatch   = pPhase->getMatch(childPos, front);

      if(pMatch)
      {
        // Note: passFrom will do nothing if we can't add to the match
        pMatch->passFrom(dragCard, *mPlayer->getHand(), front);
      }
    }

    // If dropped on discard pile, discard the drag card
    auto* pDeck = Common::findParent<DeckView>(pChild);
    if(pDeck == mDiscardView)
    {
      mPlayer->discard(dragCard);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // playerUpdate
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::playerUpdate(sPlayer pPlayer)
  {
    // Find player controls
    auto it = mPlayerControls.find(pPlayer);
    if(it == mPlayerControls.end())
    {
      mLogger->error("GameWidget::%s - Player not initialized", __func__);
      return;
    }

    // Check if players have quit
    GameState gameState = mGame->state();
    if(gameState == GAME_QUIT)
    {
      quit();
      return;
    }

    // Check if at the end of a round
    bool roundOver = (gameState == GAME_ROUND_END || gameState == GAME_OVER);
    mDealButton->setVisible(roundOver);
    mDrawButton->setVisible(!roundOver);

    // Update labels
    auto& controls = it->second;
    controls.phaseLabel->setText(pPlayer->getPhaseString().c_str());
    controls.scoreLabel->setText(("Score: " + std::to_string(pPlayer->getScore())).c_str());

    if(mGame->winner() == pPlayer)
    {
      controls.stateLabel->setText("WINNER");
    }
    else if(pPlayer->isSkipped())
    {
      controls.stateLabel->setText("SKIPPED");
    }
    else
    {
      switch(pPlayer->getState())
      {
        default:
        case PLAYER_INIT:
        case PLAYER_INACTIVE:
          controls.stateLabel->setText("IDLE");
          break;

        case PLAYER_IDLE:
          controls.stateLabel->setText("DRAW CARD");
          break;

        case PLAYER_ACTIVE:
          controls.stateLabel->setText("ACTIVE");
          break;

        case PLAYER_SKIP:
          controls.stateLabel->setText("SKIP PLAYER");
          break;

        case PLAYER_OUT:
          controls.stateLabel->setText("OUT");
          break;

        case PLAYER_QUIT:
          controls.stateLabel->setText("QUIT");
          controls.phaseLabel->setVisible(false);
          controls.scoreLabel->setVisible(false);
          if(controls.skipButton)
          {
            controls.skipButton->setVisible(false);
          }
          break;
      }
    }

    // If current player
    if(pPlayer == mPlayer)
    {
      GameState state = mPlayer->getState();
      if(state == PLAYER_IDLE)
      {
        mDrawButton->setEnabled(true);
        mDiscardButton->setEnabled(mPlayer->canDrawFromDiscard());
      }

      for(auto& pair : mPlayerControls)
      {
        auto& otherPlayer = pair.first;
        auto& skipButton  = pair.second.skipButton;
        if(skipButton)
        {
          skipButton->setEnabled(state == PLAYER_SKIP && !otherPlayer->isSkipped());
        }
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setDragLoc
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::setDragLoc(const QPoint& pos)
  {
    static constexpr QPoint CENTER_OFFSET(CardImage::SCALE_WIDTH / 2, CardImage::SCALE_HEIGHT / 2);
    mDragView->move(pos - CENTER_OFFSET);
  }

  /////////////////////////////////////////////////////////////////////////////
  // updateDiscard
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::updateDiscard()
  {
    Card topCard;
    bool valid = mGame->getDiscardPileTopCard(topCard);
    mDiscardDeck->pop();
    if(valid)
    {
      mDiscardDeck->add(topCard);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // updateDraw
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::updateDraw()
  {
    static const Card DRAW_CARD;
    bool empty = (mGame->numCardsInDrawPile() == 0);
    if(empty != mDrawDeck->empty())
    {
      if(empty)
      {
        mDrawDeck->pop();
      }
      else
      {
        mDrawDeck->add(DRAW_CARD);
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // updatePlayerHand
  /////////////////////////////////////////////////////////////////////////////
  void GameWidget::updatePlayerHand()
  {
    mPlayerHand->setCards(*mPlayer->getHand());
  }

} // namespace Phase10
