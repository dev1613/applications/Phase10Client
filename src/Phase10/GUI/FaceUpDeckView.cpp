/**
 * @file Phase10FaceUpDeckView.cpp
 *
 * @brief Classes to represent graphics view that displays a face up deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <QMouseEvent>
#include <QToolTip>

#include <Phase10/Deck.h>
#include <Phase10/GUI/CardImage.h>
#include <Phase10/GUI/FaceUpDeckView.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  FaceUpDeckView::FaceUpDeckView(System::sLogger pLogger, sDeck pHand, size_t fitCards, QWidget *parent) :
    DeckView(pLogger, pHand, fitCards, parent)
  {
    refreshScene(pHand);
    setMouseTracking(true);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  FaceUpDeckView::FaceUpDeckView(System::sLogger pLogger, sDeck pHand, QWidget *parent) :
    FaceUpDeckView(pLogger, pHand, 10, parent)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // getCardImage
  /////////////////////////////////////////////////////////////////////////////
  scCardImage FaceUpDeckView::getCardImage(const Card& card) const
  {
    return CardImageFactory::getInstance().getCardImage(card);
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseMoveEvent
  /////////////////////////////////////////////////////////////////////////////
  void FaceUpDeckView::mouseMoveEvent(QMouseEvent *pEvent)
  {
    Card card;
    if(pEvent->buttons() == Qt::NoButton && getCardAtPos(pEvent->localPos(), card))
    {
      QToolTip::showText(pEvent->globalPos(), card.toString().c_str());
    }
    else
    {
      QToolTip::hideText();
    }

    DeckView::mouseMoveEvent(pEvent);
  }

} // namespace Phase10
