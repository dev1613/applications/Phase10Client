/**
 * @file Phase10PhaseView.cpp
 *
 * @brief Classes to represent main Phase 10 window
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <QGridLayout>
#include <QLabel>

#include <Common/GUI_Utilities.h>
#include <Phase10/Client/PlayerObject.h>
#include <Phase10/GUI/CardImage.h>
#include <Phase10/GUI/FaceUpDeckView.h>
#include <Phase10/GUI/PhaseView.h>
#include <Phase10/Match.h>
#include <Phase10/Phase.h>
#include <Phase10/Player.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  PhaseView::PhaseView(System::sLogger pLogger, PlayerObject* pPlayer, QWidget *parent) : 
    QWidget(parent), mLogger(pLogger)
  {
    initLayout();
    connect(pPlayer, &PlayerObject::playerUpdate, this, &PhaseView::playerUpdate);
    playerUpdate(pPlayer->player());
  }

  /////////////////////////////////////////////////////////////////////////////
  // getMatch
  /////////////////////////////////////////////////////////////////////////////
  sMatch PhaseView::getMatch(const QPoint& pos) const
  {
    bool front = true;
    return getMatch(pos, front);
  }

  /////////////////////////////////////////////////////////////////////////////
  // getMatch
  /////////////////////////////////////////////////////////////////////////////
  sMatch PhaseView::getMatch(const QPoint& pos, bool& front) const
  {
    sMatch pMatch;
    auto* pDeckView = Common::findChildAt<DeckView>(this, pos);
    if(pDeckView)
    {
      auto it = mMatches.find(pDeckView);
      if(it != mMatches.end())
      {
        pMatch = it->second;

        // Check for front/back
        auto centerPos = pDeckView->rect().center();
        auto localPos  = pDeckView->mapFrom(this, pos);

        front = (localPos.x() < centerPos.x());
      }
    }

    return pMatch;
  }

  /////////////////////////////////////////////////////////////////////////////
  // initLayout
  /////////////////////////////////////////////////////////////////////////////
  void PhaseView::initLayout()
  {
    auto* layout = new QGridLayout(this);
    layout->setHorizontalSpacing(CardImage::SCALE_WIDTH / 3);
    layout->setVerticalSpacing(5);
    setLayout(layout);
  }

  /////////////////////////////////////////////////////////////////////////////
  // playerUpdate
  /////////////////////////////////////////////////////////////////////////////
  void PhaseView::playerUpdate(sPlayer pPlayer)
  {
    // No-op if phase has not changed
    if(pPlayer->getPhase() == mPhase)
    {
      return;
    }

    // Update phase
    mLogger->debug("PhaseView: Update phase for %s", pPlayer->name().c_str());
    mPhase = pPlayer->getPhase();
    mMatches.clear();

    // Clear layout
    auto* gridLayout = dynamic_cast<QGridLayout*>(layout());
    QLayoutItem *item;
    while((item = gridLayout->takeAt(0)) != nullptr)
    {
      item->widget()->deleteLater();
      delete item;
    }

    // Add new matches
    int col = 0;
    for(auto& pMatch : mPhase->matches())
    {
      auto* matchLabel = new QLabel(pMatch->name().c_str(), this);
      gridLayout->addWidget(matchLabel, 0, col, Qt::AlignCenter);

      auto* matchView = new FaceUpDeckView(mLogger, pMatch->getDeck(), pMatch->minSize(), this);
      gridLayout->addWidget(matchView, 1, col, Qt::AlignCenter);

      mMatches[matchView] = pMatch;
      ++col;
    }
  }

} // namespace Phase10