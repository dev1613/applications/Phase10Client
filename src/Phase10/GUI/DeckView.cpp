/**
 * @file Phase10DeckView.cpp
 *
 * @brief Classes to represent graphics view that displays a deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <algorithm>

#include <QMouseEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

#include <Phase10/Client/DeckObject.h>
#include <Phase10/Deck.h>
#include <Phase10/GUI/DeckView.h>

namespace Phase10
{
  // Default spacing for card
  static constexpr int    BORDER_WIDTH = 3;
  static constexpr double INCREMENT    = CardImage::SCALE_WIDTH / 3.0;
  static constexpr double THRESHOLD    = INCREMENT / 100.0;

  /////////////////////////////////////////////////////////////////////////////
  // getIncrement
  /////////////////////////////////////////////////////////////////////////////
  static inline double getIncrement(size_t numCards, double maxWidth)
  {
    if(numCards <= 0 || maxWidth <= CardImage::SCALE_WIDTH)
    {
      return 0.0;
    }
    else
    {
      double maxIncr = (maxWidth - CardImage::SCALE_WIDTH) / static_cast<double>(numCards - 1);
      return std::min(INCREMENT, maxIncr);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // getWidth
  /////////////////////////////////////////////////////////////////////////////
  static inline double getWidth(size_t numCards)
  {
    if(numCards <= 0)
    {
      return 0.0;
    }
    else
    {
      return(static_cast<double>(numCards - 1) * INCREMENT + CardImage::SCALE_WIDTH);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  DeckView::DeckView(System::sLogger pLogger, sDeck pDeck, size_t fitCards, QWidget *parent) :
    QGraphicsView(parent), mLogger(pLogger), mMaxWidth(getWidth(fitCards))
  {
    initScene();
    auto* deckObj = new DeckObject(pDeck, this);
    connect(deckObj, &DeckObject::deckUpdate, this, &DeckView::refreshScene);
  }

  /////////////////////////////////////////////////////////////////////////////
  // refreshScene
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::refreshScene(sDeck pDeck)
  {
    //TODO: Add tracking to keep pixmap items for cards that haven't changed

    // Clear previous scene
    mScene->clear();
    mCardOffset.clear();

    // Get increment for squeezing cards
    auto&  deck      = *pDeck;
    size_t numCards  = deck.size();
    double increment = getIncrement(numCards, mMaxWidth);
    double width     = std::min(getWidth(numCards), mMaxWidth);
    double offset    = (mMaxWidth - width) * 0.5;

    // Draw cards
    for(auto& card : deck)
    {
      // Draw card
      auto pImage = getCardImage(card);
      if(pImage && pImage->loaded())
      {
        auto* item = mScene->addPixmap(pImage->pixmap());
        item->setPos(offset, 0.0);
      }
      else
      {
        mLogger->error("DeckView: Failed to load image for %s", card.toString().c_str());
      }

      // Increment offset
      if(increment < THRESHOLD)
      {
        break;
      }
      else
      {
        offset += increment;
        mCardOffset[offset] = card;
      }
    }

    // Expand window for last card to end of window
    if(numCards > 0)
    {
      mCardOffset[mMaxWidth] = deck.back();
    }

    // Refresh layout
    updateGeometry();
  }

  /////////////////////////////////////////////////////////////////////////////
  // getCardAtPos
  /////////////////////////////////////////////////////////////////////////////
  bool DeckView::getCardAtPos(const QPointF& pos, Card& card) const
  {
    bool success = false;
    auto it = mCardOffset.upper_bound(pos.x());
    if(it != mCardOffset.end())
    {
      card = it->second;
      success = true;
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // initScene
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::initScene()
  {
    mScene = new QGraphicsScene;
    setScene(mScene);
    setFixedSize(mMaxWidth + BORDER_WIDTH, CardImage::SCALE_HEIGHT + BORDER_WIDTH);
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseDoubleClickEvent
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::mouseDoubleClickEvent(QMouseEvent *pEvent)
  {
    QGraphicsView::mouseDoubleClickEvent(pEvent);
    Card card;
    if(pEvent->buttons() == Qt::LeftButton && getCardAtPos(pEvent->localPos(), card))
    {
      mLogger->debug("DeckView: Double clicked %s", card.toString().c_str());
      cardDoubleClicked(card);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseMoveEvent
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::mouseMoveEvent(QMouseEvent *pEvent)
  {
    pEvent->ignore();
    QGraphicsView::mouseMoveEvent(pEvent);
  }

  /////////////////////////////////////////////////////////////////////////////
  // mousePressEvent
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::mousePressEvent(QMouseEvent *pEvent)
  {
    QGraphicsView::mousePressEvent(pEvent);
    Card card;
    if(pEvent->buttons() == Qt::LeftButton && getCardAtPos(pEvent->localPos(), card))
    {
      mLogger->debug("DeckView: Pressed %s", card.toString().c_str());
      cardPressed(card);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // mouseReleaseEvent
  /////////////////////////////////////////////////////////////////////////////
  void DeckView::mouseReleaseEvent(QMouseEvent *pEvent)
  {
    pEvent->ignore();
    QGraphicsView::mouseReleaseEvent(pEvent);
  }

} // namespace Phase10
