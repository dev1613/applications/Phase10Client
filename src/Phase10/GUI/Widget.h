/**
 * @file Widget.h
 *
 * @brief Main widget class for Phase 10 client GUI
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <memory>

#include <QWidget>

#include <Phase10/CommonTypes.h>
#include <System/Logger.h>

// Forward declarations
class QDialog;

namespace Phase10
{
  // Forward declarations
  class ClientManager;
  class GameWidget;

  //-------------------------------------------------------------------------//
  // Class:       Widget
  // Description: Main widget class for Phase 10 client
  //-------------------------------------------------------------------------//
  class Widget : public QWidget
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param configFile - manager config file
    /// @param pGameFactory - game factory
    /// @param pUserFactory - user factory
    ///////////////////////////////////////////////////////////////////////////
    Widget(System::sLogger pLogger, const std::string& configFile,
           sGameFactory pGameFactory, sUserFactory pUserFactory);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Widget() = default;

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    std::shared_ptr<ClientManager> mManager; ///< client manager

    QDialog*    mErrorDialog;     ///< dialog for connection error
    QDialog*    mOpenGamesDialog; ///< dialog for displaying open games
    QDialog*    mQuitDialog;      ///< dialog for losing too many players
    QDialog*    mRegisterDialog;  ///< dialog for registering user
    GameWidget* mGameWidget;      ///< active game widget

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the connection with the server is lost
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void connectionLost();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates the error dialog box
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void createErrorDialog();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates the game widget
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void createGameWidget();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates the open games dialog box
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void createOpenGamesDialog();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates the quit dialog box
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void createQuitDialog();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates the user registration dialog box
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void createRegisterDialog();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when too many players leave the game
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameQuit();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the game is started
    /// @param pGame - game started
    /// @param pPlayer - player associated with this client
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameStarted(sGame pGame, sPlayer pPlayer);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the GUI layout
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initializeLayout();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the widget is resized
    /// @param pEvent - resize event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void resizeEvent(QResizeEvent* pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the client manager
    /// @param configFile - manager config file
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void startManager(const std::string& configFile);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the user is registered
    /// @param pUser - new user
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void userRegistered(sUser pUser);
  };

} // namespace Phase10
