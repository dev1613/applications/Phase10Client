/**
 * @file GameWidget.h
 *
 * @brief Classes to represent main Phase 10 window
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>

#include <QWidget>

#include <Phase10/CommonTypes.h>
#include <System/Logger.h>

// Forward declarations
class QLabel;
class QPushButton;

namespace Phase10
{
  class DeckView;

  //-------------------------------------------------------------------------//
  // Class:       PlayerControls
  // Description: Controls for player
  //-------------------------------------------------------------------------//
  struct PlayerControls
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    PlayerControls() :
      phaseLabel(nullptr),
      stateLabel(nullptr),
      scoreLabel(nullptr),
      skipButton(nullptr)
    {
    }

    QLabel*      phaseLabel; ///< label for phase number
    QLabel*      stateLabel; ///< label for player state
    QLabel*      scoreLabel; ///< label for player score
    QPushButton* skipButton; ///< button to skip this player (nullptr for me)
  };

  //-------------------------------------------------------------------------//
  // Class:       GameWidget
  // Description: Main Phase 10 game widget
  //-------------------------------------------------------------------------//
  class GameWidget : public QWidget
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    GameWidget(System::sLogger pLogger, QWidget* parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~GameWidget() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initialize game
    /// @param pGame - game state
    /// @param pPlayer - this player
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initializeGame(sGame pGame, sPlayer pPlayer);

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the deal button is pressed
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void deal();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the game loses too many players
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void quit();

  private Q_SLOTS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called a card from my hand is double clicked
    /// @param card - card that was double clicked
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void cardDoubleClicked(const Card& card);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called a card from my hand is pressed
    /// @param card - card that was pressed
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void cardPressed(const Card& card);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the draw button is pressed
    /// @param fromDiscard - T: draw from discard pile, F: draw from draw pile
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void draw(bool fromDiscard);

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    sGame   mGame;   ///< current game
    sPlayer mPlayer; ///< active player

    QPushButton *mDrawButton;    ///< draw button
    QPushButton *mDiscardButton; ///< draw from discard button
    QPushButton *mDealButton;    ///< deal button
    DeckView    *mDiscardView;   ///< discard pile view

    sDeck mDiscardDeck; ///< placeholder for discard pile
    sDeck mDrawDeck;    ///< placeholder for draw pile
    sDeck mPlayerHand;  ///< placeholder for player hand

    sDeck     mDragDeck; ///< card currently being dragged
    DeckView *mDragView; ///< viewer for drag deck

    std::map<sPlayer, PlayerControls> mPlayerControls; ///< player controls

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a GameWidget for the player
    /// @param pPlayer - player to add
    /// @param topRow - is player on top row?
    /// @return - GameWidget for player
    ///////////////////////////////////////////////////////////////////////////
    void createPlayer(sPlayer pPlayer, int row, int col);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the layout
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initializeLayout();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is moved
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void mouseMoveEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is released
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void mouseReleaseEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the player state is updated
    /// @param pPlayer - player that was updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void playerUpdate(sPlayer pPlayer);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the location of the drag card
    /// @param pos - location of drag card
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setDragLoc(const QPoint& pos);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the discard pile is updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void updateDiscard();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the draw pile is updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void updateDraw();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the player hand is updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void updatePlayerHand();
  };

} // namespace Phase10
