/**
 * @file Phase10CardImage.cpp
 *
 * @brief Classes to load Phase 10 card images
 *
 * @ingroup Phase 10 GUI
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <filesystem>
#include <QImage>

#include <Phase10/Card.h>
#include <Phase10/GUI/CardImage.h>

// Constants
static const std::string EXTENSION(".jpeg");
static const std::string FACE_DOWN("FaceDown");
static const std::string SKIP("Skip");
static const std::string WILD("Wild");

//TODO: Add init step to initialize resource dir
static const std::filesystem::path RESOURCE_PARENT_DIR("..");
static const std::filesystem::path RESOURCE_DIR(RESOURCE_PARENT_DIR / "resource");

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  CardImage::CardImage(const Card& card) : mLoaded(false)
  {
    // Get filename
    bool valid = true;
    std::string name;
    if(card.isStandard())
    {
      name = ColorToString(card.getColor()) + std::to_string(card.getValue());
    }
    else if(card.isSkip())
    {
      name = SKIP;
    }
    else if(card.isWild())
    {
      name = WILD;
    }
    else
    {
      valid = false;
    }

    // Read image
    if(valid)
    {
      load(name);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  CardImage::CardImage(const std::string& filename) : mLoaded(false)
  {
    load(filename);
  }

  /////////////////////////////////////////////////////////////////////////////
  // load
  /////////////////////////////////////////////////////////////////////////////
  void CardImage::load(const std::string& filename)
  {
    auto path = RESOURCE_DIR / filename;
    QImage image;
    if(image.load(path.string().c_str()))
    {
      mPixmap = QPixmap::fromImage(image.scaled(SCALE_WIDTH, SCALE_HEIGHT));
      mLoaded = true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  CardImageFactory::CardImageFactory()
  {
    mFaceDown.reset(new CardImage(FACE_DOWN));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getCardImage
  /////////////////////////////////////////////////////////////////////////////
  scCardImage CardImageFactory::getCardImage(const Card& card)
  {
    // Get current instance or add instance
    auto& pImage = mImages[card];
    if(pImage == nullptr)
    {
      pImage.reset(new CardImage(card));
    }

    return pImage;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getInstance (static)
  /////////////////////////////////////////////////////////////////////////////
  CardImageFactory& CardImageFactory::getInstance()
  {
    static CardImageFactory instance;
    return instance;
  }

} // namespace Phase10
