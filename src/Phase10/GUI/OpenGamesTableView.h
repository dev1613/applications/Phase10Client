/**
 * @file OpenGamesTableView.h
 *
 * @brief Table view that displays the open games
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>

#include <QTableWidget>

#include <Phase10/Client/Manager.h>
#include <System/Logger.h>

// Forward declarations
class QLabel;
class QPushButton;

namespace Common
{
  class EntryBox;
}

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       OpenGamesTableRow
  // Description: Holds the widget items in the table row 
  //-------------------------------------------------------------------------//
  struct OpenGamesTableRow
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    OpenGamesTableRow() :
      row(-1),
      playerCountLabel(nullptr),
      stateLabel(nullptr),
      joinButton(nullptr)
    {
    }

    // Struct variables
    int          row;              ///< row number
    QLabel*      playerCountLabel; ///< label for number of players
    QLabel*      stateLabel;       ///< label for status
    QPushButton* joinButton;       ///< button for joining
    QPushButton* startButton;      ///< button for starting game
  };

  //-------------------------------------------------------------------------//
  // Class:       OpenGamesTableView
  // Description: Table view that displays open games
  //-------------------------------------------------------------------------//
  class OpenGamesTableView : public QTableWidget
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pManager - client manager
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    OpenGamesTableView(System::sLogger pLogger, std::shared_ptr<ClientManager> pManager,
                       QWidget* parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~OpenGamesTableView() = default;

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    const std::shared_ptr<ClientManager> mManager; ///< client manager

    std::map<uint32_t, OpenGamesTableRow> mGameRows; ///< game ID -> row

    QPushButton*      mCreateButton;   ///< button to create new game
    Common::EntryBox* mCreateEntryBox; ///< entry box for new game name

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the game is joined
    /// @param pGame - game joined
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void gameJoined(sGame pGame);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the layout items
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initializeLayout();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the open games are updated
    /// @param pOpenGames - open games
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void openGamesUpdated(ClientManager::scOpenGamesList pOpenGames);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Updates the table data for the requested game
    /// @param gameInfo - game ID info
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void updateGame(const GameInfo& gameInfo);
  };

} // namespace Phase10
