/**
 * @file OpenGamesTableView.cpp
 *
 * @brief Table view that displays the open games
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <set>

#include <QHeaderView>
#include <QLabel>
#include <QPushButton>

#include <Common/EntryBox.h>
#include <Phase10/Game.h>
#include <Phase10/GUI/OpenGamesTableView.h>

// Constants
static constexpr int CREATE_ROW = 0;

enum
{
  COL_NAME = 0,
  COL_PLAYERS,
  COL_STATE,
  COL_ACTION,
  COL_COUNT
};

static const QStringList COL_LABELS({
    "Name", "#/Players", "State", ""
});

static const QString STATUS_ERROR_TEXT = "Invalid name";
static const QString STATUS_FAIL_TEXT  = "Internal error";

static const QString STATE_OPEN   = "Open";
static const QString STATE_FULL   = "Full";
static const QString STATE_ACTIVE = "Active";

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  OpenGamesTableView::OpenGamesTableView(System::sLogger pLogger, std::shared_ptr<ClientManager> pManager, QWidget* parent) :
      QTableWidget(parent), mLogger(pLogger), mManager(pManager)
  {
    initializeLayout();
  }

  /////////////////////////////////////////////////////////////////////////////
  // gameJoined
  /////////////////////////////////////////////////////////////////////////////
  void OpenGamesTableView::gameJoined(sGame pGame)
  {
    if(pGame)
    {
      mLogger->debug("OpenGamesTableView::%s - Joined game %s [%u]",
        __func__, pGame->name().c_str(), pGame->id());

      mCreateButton->setEnabled(false);
      mCreateEntryBox->setEnabled(false);
      updateGame(pGame->info());

      // Disable other join buttons once we've joined a game
      for(auto& pair : mGameRows)
      {
        if(pair.first != pGame->id())
        {
          pair.second.joinButton->setEnabled(false);
        }
      }

      resizeColumnsToContents();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // initializeLayout
  /////////////////////////////////////////////////////////////////////////////
  void OpenGamesTableView::initializeLayout()
  {
    // Initialize table
    setRowCount(1);
    setColumnCount(COL_COUNT);
    setHorizontalHeaderLabels(COL_LABELS);
    setMinimumWidth(500);
    setSelectionMode(QAbstractItemView::NoSelection);

    // Add create row
    mCreateEntryBox = new Common::EntryBox(this);
    setCellWidget(CREATE_ROW, COL_NAME, mCreateEntryBox);

    mCreateButton = new QPushButton("Create", this);
    setCellWidget(CREATE_ROW, COL_ACTION, mCreateButton);

    auto* statusLabel = new QLabel(this);
    setCellWidget(CREATE_ROW, COL_STATE, statusLabel);

    // Function to create game
    auto createGame = [this, statusLabel]()
    {
      std::string name = mCreateEntryBox->toPlainText().toStdString();
      if(name.empty())
      {
        statusLabel->setText(STATUS_ERROR_TEXT);
      }
      else if(!mManager->requestNewGame(name))
      {
        statusLabel->setText(STATUS_FAIL_TEXT);
      }
      else
      {
        statusLabel->setText("");
        mCreateEntryBox->setEnabled(false);
        mCreateButton->setEnabled(false);
      }

      resizeColumnsToContents();
    };

    // Set connections
    connect(mCreateEntryBox, &Common::EntryBox::editingFinished, createGame);
    connect(mCreateButton, &QPushButton::clicked, createGame);
    connect(mManager.get(), &ClientManager::gameJoined, this, &OpenGamesTableView::gameJoined);
    connect(mManager.get(), &ClientManager::openGamesUpdated, this, &OpenGamesTableView::openGamesUpdated);

    // Add initial open games
    auto pOpenGames = mManager->getOpenGames();
    if(pOpenGames)
    {
      openGamesUpdated(pOpenGames);
    }

    resizeColumnsToContents();
  }

  /////////////////////////////////////////////////////////////////////////////
  // openGamesUpdated
  /////////////////////////////////////////////////////////////////////////////
  void OpenGamesTableView::openGamesUpdated(ClientManager::scOpenGamesList pOpenGames)
  {
    mLogger->debug("OpenGamesTableView::%s - %lu open games", __func__, pOpenGames->size());

    // Get previously open games
    std::set<uint32_t> gamesToHide;
    for(auto& pair : mGameRows)
    {
      gamesToHide.insert(pair.first);
    }

    // Add/update open games
    for(auto& game : *pOpenGames)
    {
      gamesToHide.erase(game.id());
      updateGame(game);
    }

    // Hide rows for games that don't exist anymore
    for(const uint32_t& gameID : gamesToHide)
    {
      mLogger->debug("OpenGamesTableView::%s - Hiding %u (row %d)",
        __func__, gameID, mGameRows[gameID].row);
      hideRow(mGameRows[gameID].row);
    }

    resizeColumnsToContents();
  }

  /////////////////////////////////////////////////////////////////////////////
  // updateGame
  /////////////////////////////////////////////////////////////////////////////
  void OpenGamesTableView::updateGame(const GameInfo& gameInfo)
  {
    // Check if this is my game
    auto pGame = mManager->getGame();
    bool joined = (pGame != nullptr);
    bool currentGame = (joined && pGame->id() == gameInfo.id());

    // Add tabel row if it doesn't exist
    auto& tableRow = mGameRows[gameInfo.id()];
    if(tableRow.row <= CREATE_ROW)
    {
      tableRow.row = rowCount();
      setRowCount(tableRow.row + 1);

      auto* nameLabel = new QLabel(gameInfo.name().c_str(), this);
      setCellWidget(tableRow.row, COL_NAME, nameLabel);

      tableRow.playerCountLabel = new QLabel(this);
      setCellWidget(tableRow.row, COL_PLAYERS, tableRow.playerCountLabel);

      tableRow.stateLabel = new QLabel(this);
      setCellWidget(tableRow.row, COL_STATE, tableRow.stateLabel);

      tableRow.joinButton = new QPushButton("Join", this);
      tableRow.startButton = new QPushButton("Start", this);
      setCellWidget(tableRow.row, COL_ACTION, tableRow.joinButton);

      connect(tableRow.joinButton, &QPushButton::clicked, std::bind(&ClientManager::requestJoin, mManager.get(), gameInfo.id()));
      connect(tableRow.startButton, &QPushButton::clicked, std::bind(&ClientManager::requestDeal, mManager.get()));
    }

    // Update labels
    tableRow.playerCountLabel->setText(std::to_string(gameInfo.numPlayers()).c_str());

    if(gameInfo.canAddPlayers())
    {
      tableRow.stateLabel->setText(STATE_OPEN);
    }
    else if(gameInfo.full())
    {
      tableRow.stateLabel->setText(STATE_FULL);
    }
    else
    {
      tableRow.stateLabel->setText(STATE_ACTIVE);
    }

    // Update join/start buttons
    if(currentGame)
    {
      tableRow.startButton->setEnabled(gameInfo.ready());
      setCellWidget(tableRow.row, COL_ACTION, tableRow.startButton);
    }
    else
    {
      tableRow.joinButton->setEnabled(!joined && gameInfo.canAddPlayers());
      setCellWidget(tableRow.row, COL_ACTION, tableRow.joinButton);
    }
  }

} // namespace Phase10
