/**
 * @file Phase10FaceUpDeckView.h
 *
 * @brief Classes to represent graphics view that displays a face up deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <Phase10/GUI/DeckView.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       FaceUpDeckView
  // Description: Graphics view for displaying a face up deck
  //-------------------------------------------------------------------------//
  class FaceUpDeckView : public DeckView
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pHand - hand to be displayed
    /// @param fitCards - if number of cards > fitCards, squeeze together
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    FaceUpDeckView(System::sLogger pLogger, sDeck pHand, size_t fitCards = 10, QWidget *parent = nullptr);
    FaceUpDeckView(System::sLogger pLogger, sDeck pHand, QWidget *parent);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~FaceUpDeckView() = default;

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the card image for the specified card
    /// @param card - card to get
    /// @return - card image
    ///////////////////////////////////////////////////////////////////////////
    virtual scCardImage getCardImage(const Card& card) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is moved
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void mouseMoveEvent(QMouseEvent *pEvent) override;
  };

} // namespace Phase10
