/**
 * @file Phase10DeckView.h
 *
 * @brief Classes to represent graphics view that displays a deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>

#include <QGraphicsView>

#include <Phase10/CommonTypes.h>
#include <Phase10/GUI/CardImage.h>
#include <System/Logger.h>

// Forward declarations
class QGraphicsScene;

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       DeckView
  // Description: Graphics view for displaying a deck
  //-------------------------------------------------------------------------//
  class DeckView : public QGraphicsView
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pDeck - deck to be displayed
    /// @param fitCards - if number of cards > fitCards, squeeze together
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    DeckView(System::sLogger pLogger, sDeck pDeck, size_t fitCards, QWidget *parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~DeckView() = default;

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Signal for card being double clicked
    /// @param card - card that was double clicked
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void cardDoubleClicked(const Card& card);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Signal for card being pressed
    /// @param card - card that was pressed
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void cardPressed(const Card& card);

  protected:
    // Protected class variables
    const System::sLogger mLogger; ///< debug logger

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the card at the specified local position
    /// @param pos - local position
    /// @param card[out] - card at that position
    /// @return - true if card is valid, else false
    ///////////////////////////////////////////////////////////////////////////
    bool getCardAtPos(const QPointF& pos, Card& card) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the card image for the specified card
    /// @param card - card to get
    /// @return - card image
    ///////////////////////////////////////////////////////////////////////////
    virtual scCardImage getCardImage(const Card& card) const = 0;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is double clicked
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void mouseDoubleClickEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is moved
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void mouseMoveEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is pressed within the view
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void mousePressEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the mouse is released
    /// @param pEvent - mouse event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void mouseReleaseEvent(QMouseEvent *pEvent) override;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Refreshes the graphics scene
    /// @param pDeck - deck updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void refreshScene(sDeck pDeck);

  private:
    // Private class variables
    const double    mMaxWidth; ///< maximum hand width
    QGraphicsScene *mScene;    ///< graphics scene

    std::map<double, Card> mCardOffset; ///< offset : card

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the graphics scene
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initScene();
  };

} // namespace Phase10
