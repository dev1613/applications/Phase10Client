/**
 * @file Phase10FaceDownDeckView.h
 *
 * @brief Classes to represent graphics view that displays a face down deck
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <Phase10/GUI/DeckView.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       FaceDownDeckView
  // Description: Graphics view for displaying a face down deck
  //-------------------------------------------------------------------------//
  class FaceDownDeckView : public DeckView
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pHand - hand to be displayed
    /// @param fitCards - if number of cards > fitCards, squeeze together
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    FaceDownDeckView(System::sLogger pLogger, sDeck pHand, size_t fitCards = 10, QWidget *parent = nullptr);
    FaceDownDeckView(System::sLogger pLogger, sDeck pHand, QWidget *parent);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~FaceDownDeckView() = default;

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the card image for the specified card
    /// @param card - card to get
    /// @return - card image
    ///////////////////////////////////////////////////////////////////////////
    virtual scCardImage getCardImage(const Card& card) const;
  };

} // namespace Phase10
