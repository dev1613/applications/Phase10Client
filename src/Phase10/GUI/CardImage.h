/**
 * @file Phase10CardImage.h
 *
 * @brief Classes to load Phase 10 card images
 *
 * @ingroup Phase 10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>
#include <memory>
#include <QPixmap>

#include <Phase10/Card.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       CardImage
  // Description: Phase 10 card images
  //-------------------------------------------------------------------------//
  class CardImage
  {
  public:
    friend class CardImageFactory;

    //TODO: Make these editable by init
    // Constants
    static constexpr size_t SCALE_WIDTH  = 80;
    static constexpr size_t SCALE_HEIGHT = 125;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~CardImage() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if image is loaded
    /// @return - true if loaded, else false
    ///////////////////////////////////////////////////////////////////////////
    bool loaded() const { return mLoaded; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the card image pixmap
    /// @return - card image pixmap
    ///////////////////////////////////////////////////////////////////////////
    const QPixmap& pixmap() const { return mPixmap; }

  private:
    // Private class variables
    bool    mLoaded; ///< image loaded?
    QPixmap mPixmap; ///< loaded image pixmap

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor (only accessible by factory)
    /// @param card - card for which to load image
    ///////////////////////////////////////////////////////////////////////////
    CardImage(const Card& card);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor (only accessible by factory)
    /// @param filename - image file name
    ///////////////////////////////////////////////////////////////////////////
    CardImage(const std::string& filename);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Loads the image from the filename
    /// @param filename - image file name
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void load(const std::string& filename);
  };

  // Typedefs
  using sCardImage  = std::shared_ptr<CardImage>;
  using scCardImage = std::shared_ptr<const CardImage>;

  //-------------------------------------------------------------------------//
  // Class:       CardImageFactory
  // Description: Factory for Phase 10 card images
  //-------------------------------------------------------------------------//
  class CardImageFactory
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~CardImageFactory() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the image for a card
    /// @param card - card for which to get image
    /// @return - image for card, or nullptr if unsuccessful
    ///////////////////////////////////////////////////////////////////////////
    scCardImage getCardImage(const Card& card);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the image for the face down card
    /// @return - image for face down card, or nullptr if unsuccessful
    ///////////////////////////////////////////////////////////////////////////
    scCardImage getFaceDown() const { return mFaceDown; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the singleton instance
    /// @return - singleton instance
    ///////////////////////////////////////////////////////////////////////////
    static CardImageFactory& getInstance();

  private:
    // Private class variables
    std::map<Card, scCardImage> mImages; ///< card images

    scCardImage mFaceDown; ///< face down card image

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor (only accessible from singleton)
    ///////////////////////////////////////////////////////////////////////////
    CardImageFactory();
  };

} // namespace Phase10
