/**
 * @file Phase10PhaseView.h
 *
 * @brief Classes to represent main Phase 10 window
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <map>

#include <QWidget>

#include <System/Logger.h>
#include <Phase10/CommonTypes.h>

namespace Phase10
{
  // Forward declarations
  class PlayerObject;

  //-------------------------------------------------------------------------//
  // Class:       PhaseView
  // Description: Group box for displaying a phase
  //-------------------------------------------------------------------------//
  class PhaseView : public QWidget
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param pPlayer - player associated with this phase
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    PhaseView(System::sLogger pLogger, PlayerObject* pPlayer, QWidget *parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~PhaseView() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the match at the given position
    /// @param pos - local position of match
    /// @param front - true for dropping at front, false for dropping at back
    /// @return - match at the position, or nullptr if none
    ///////////////////////////////////////////////////////////////////////////
    sMatch getMatch(const QPoint& pos) const;
    sMatch getMatch(const QPoint& pos, bool& front) const;

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    sPhase mPhase; ///< current phase

    std::map<const DeckView*, sMatch> mMatches; ///< viewers for matches

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Initializes the widget layout
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void initLayout();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when the player state is updated
    /// @param pPlayer - player updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void playerUpdate(sPlayer pPlayer);
  };

} // namespace Phase10
