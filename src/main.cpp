/**
 * @file main.cpp
 *
 * @brief Main entry point for Client
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */
 
 // Includes
#include <QApplication>

#include <Phase10/GUI/Widget.h>
#include <Phase10/Game.h>
#include <Phase10/User.h>
#include <System/Logger.h>

// Constants
static const std::string DEFAULT_CONFIG_FILE("../config/ClientConfig.xml");

// Main function
int main(int argc, char *argv[])
{
  // Create logger
  auto pLogger = std::make_shared<System::Logger>();
  pLogger->setLevel(System::LOG_DEBUG);

  // Get config file
  std::string configFile;
  if(argc > 1)
  {
    configFile = argv[1];
  }
  else
  {
    configFile = DEFAULT_CONFIG_FILE;
  }

  // Create factories
  auto pGameFactory = std::make_shared<Phase10::GameFactory>(pLogger, false);
  auto pUserFactory = std::make_shared<Phase10::UserFactory>(pLogger);

  if(pGameFactory && pUserFactory)
  {
    pLogger->debug("Created factories");
  }
  else
  {
    pLogger->fatal("Failed to create factories");
    return -1;
  }

  // Start application
  QApplication app(argc, argv);
  Phase10::Widget widget(pLogger, configFile, pGameFactory, pUserFactory);
  widget.showMaximized();
  return app.exec();
} // End - main
