/**
 * @file GUI_Utilities.h
 *
 * @brief Utility functions for GUI
 *
 * @ingroup Phase10 GUI
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <QWidget>

namespace Common
{
  /////////////////////////////////////////////////////////////////////////////
  /// @brief Find a child widget of the requested type at the position
  /// @param pWidget - parent widget
  /// @param pos - position in widget coordinates
  /// @return - child widget, or nullptr if not found
  /////////////////////////////////////////////////////////////////////////////
  template <typename T>
  T* findChildAt(const QWidget* pWidget, const QPoint& pos)
  {
    return findParent<T>(pWidget->childAt(pos));
  }

  /////////////////////////////////////////////////////////////////////////////
  /// @brief Find a parent widget of the specified type
  /// @param pChild - child widget
  /// @return - parent widget, or nullptr if not found
  /////////////////////////////////////////////////////////////////////////////
  template <typename T>
  T* findParent(QWidget* pChild)
  {
    T* pResult = dynamic_cast<T*>(pChild);
    if(pChild && pResult == nullptr)
    {
      pResult = findParent<T>(pChild->parentWidget());
    }

    return pResult;
  }

} // namespace Common
