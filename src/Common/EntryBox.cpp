/**
 * @file EntryBox.cpp
 *
 * @brief Wrapper for QTextEdit that signals the enter key
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <qevent.h>

#include <Common/EntryBox.h>

namespace Common
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  EntryBox::EntryBox(QWidget* parent) : QTextEdit(parent)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  EntryBox::EntryBox(const QString& text, QWidget* parent) : QTextEdit(text, parent)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // keyPressEvent
  /////////////////////////////////////////////////////////////////////////////
  void EntryBox::keyPressEvent(QKeyEvent* e)
  {
    if(e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    {
      editingFinished();
    }
    else
    {
      QTextEdit::keyPressEvent(e);
    }
  }

} // namespace Common
