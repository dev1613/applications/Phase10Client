/**
 * @file EntryBox.h
 *
 * @brief Wrapper for QTextEdit that signals the enter key
 *
 * @ingroup Client
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <QTextEdit>

namespace Common
{
  //-------------------------------------------------------------------------//
  // Class:       EntryBox
  // Description: Wrapper for QTextEdit that signals the enter key
  //-------------------------------------------------------------------------//
  class EntryBox : public QTextEdit
  {
    Q_OBJECT

  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param text - initial text
    /// @param parent - parent widget
    ///////////////////////////////////////////////////////////////////////////
    EntryBox(QWidget* parent = nullptr);
    EntryBox(const QString& text, QWidget* parent = nullptr);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~EntryBox() = default;

  Q_SIGNALS:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Invoked when enter is pressed in the editor
    /// @retur - None
    ///////////////////////////////////////////////////////////////////////////
    void editingFinished();

  private:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a key is pressed
    /// @param e - event
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void keyPressEvent(QKeyEvent* e) override;
  };

} // namespace Common
